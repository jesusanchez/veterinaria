from django import forms
from .models import Medico, Diagnostico

class MedicoForm(forms.ModelForm):
    class Meta:
        model = Medico
        fields=['cedula','nombre','apellido','telefono','email','numero_registro_diploma']

        labels = {
            'cedula':'Cedula',
            'nombre':'Nombre',
            'apellido':'Apellido',
            'telefono':'Telefono',
            'email':'Email',
            'numero_registro_diploma':'Numero de registro de diploma',

        }

        widgets = {

            'cedula' : forms.TextInput(attrs={'class':'form-control'}),
            'nombre' : forms.TextInput(attrs={'class':'form-control'}),
            'apellido' : forms.TextInput(attrs={'class':'form-control'}),
            'telefono' : forms.TextInput(attrs={'class':'form-control'}),
            'email' : forms.TextInput(attrs={'class':'form-control'}),
            'numero_registro_diploma' : forms.TextInput(attrs={'class':'form-control'}),
            

        }

class DiagnosticoForm(forms.ModelForm):
    class Meta:
        model = Diagnostico
        fields = ['nombre_de_enfermdad','sintomas','diagnostico_1','tratamiento','medicamento','medico','mascota','solicitudCita']

        labels = {
            'nombre_de_enfermdad':'Nombre de enfermedad',
            'sintomas':'Sintomas',
            'diagnostico_1':'Diagnostico',
            'tratamiento':'Tratamiento',
            'medicamento':'Medicamentos',
            'medico':'Medico',
            'mascota':'Mascota',
            'solicitudCita':'Solicitud Cita',

        }
        
        widgets = {
            'nombre_de_enfermdad' : forms.TextInput(attrs={'class':'form-control'}),
            'sintomas' :forms.Textarea(attrs={'class': 'form-control', 'style': 'resize:none;'}),
            'diagnostico_1' : forms.Textarea(attrs={'class': 'form-control', 'style': 'resize:none;'}),
            'tratamiento' : forms.Textarea(attrs={'class': 'form-control', 'style': 'resize:none;'}),
            'medicamento' : forms.TextInput(attrs={'class':'form-control'}),
            'medico' : forms.Select(attrs={'class':'form-control'}),
            'mascota' : forms.Select(attrs={'class':'form-control'}),
            'SolicitudCita' : forms.Select(attrs={'class':'form-control'}),
            
        }