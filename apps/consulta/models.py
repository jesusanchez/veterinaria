from django.db import models
from apps.mascota.models import  Mascota
from apps.cliente.models import SolicitudCita
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.urls import reverse

# Create your models here.


class Medico(models.Model):

    cedula = models.CharField(max_length=20)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    telefono = models.CharField(max_length=15)
    email= models.EmailField(max_length=100)
    numero_registro_diploma = models.CharField(max_length=30)

    def __str__(self):
        return '{} {} '.format(self.nombre, self.apellido)

    def get_absolute_url(self):
        return reverse('medico_list')
   


class Diagnostico(models.Model):

    nombre_de_enfermdad = models.CharField(max_length=100)
    sintomas= models.TextField()
    diagnostico_1 = models.TextField()
    tratamiento = models.TextField()
    medicamento = models.CharField(max_length=200)
    medico= models.ForeignKey(Medico, null=True, blank=True, on_delete= models.CASCADE)
    mascota= models.ForeignKey(Mascota, null=True, blank=True, on_delete= models.CASCADE)
    solicitudCita= models.ForeignKey(SolicitudCita, null=True, blank=True, on_delete= models.CASCADE)

    def __str__(self):
        return self.nombre_de_enfermdad

    def get_absolute_url(self):
        return reverse('diagnostico_list')
   
    



