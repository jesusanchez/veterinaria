from rest_framework import serializers
from .models import Medico, Diagnostico

class MedicoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Medico
        fields = ('__all__')


class DiagnosticoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Diagnostico
        fields = ('__all__')

