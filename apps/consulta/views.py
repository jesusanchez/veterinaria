from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from apps.consulta.models import Medico, Diagnostico
from apps.consulta.forms import MedicoForm, DiagnosticoForm
from django.views.generic import ListView, DetailView
from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from rest_framework import viewsets
from .serializers import MedicoSerializer, DiagnosticoSerializer



# Create your views here.
@login_required
def first_view(request):
    return  render(request, 'base.html')
#----------------------------------------------Medico--------------------------------------------

@login_required
def medico_register(request):
    if request.method == "POST":
        form = MedicoForm(request.POST)
        if form.is_valid():
            medico_item = form.save(commit=False)
            medico_item.save()
            return redirect('medico_list')
    else:
            form = MedicoForm()

    return render(request,'consulta/medico_form.html',{'form':form})   


@login_required
def medico_list(request):
	medico_list = Medico.objects.all().order_by('id')
	context = {'objects_list': medico_list}
	return render(request, 'consulta/medico_list.html', context)


# se edita los datos de las mascotas
@login_required 
def medico_detail(request, medico_id):
	medico = Medico.objects.get(id=medico_id)
	if request.method == 'GET':
		form = MedicoForm(instance = medico)
	else:
		form = MedicoForm(request.POST, instance=medico)
		if form.is_valid():
			form.save()	
		return redirect('medico_list')
	return render(request, 'consulta/medico_form.html',{'form':form})
    
@method_decorator(login_required, name='dispatch')
class medicoDelete(DeleteView):

    model=Medico
    success_url = reverse_lazy('medico_list')


#-------------------------------------------Diagnostico--------------------------------------------
@login_required
def diagnostico_register(request):
    if request.method == "POST":
        form = DiagnosticoForm(request.POST)
        if form.is_valid():
            diagnostico_item = form.save(commit=False)
            diagnostico_item.save()	
            return redirect('diagnostico_list')
    else:
            form = DiagnosticoForm()
    return render(request,'consulta/diagnostico_form.html',{'form':form})         

@login_required
def diagnostico_list(request):
	diagnostico_list = Diagnostico.objects.all().order_by('id')
	context = {'objects_list': diagnostico_list}
	return render(request, 'consulta/diagnostico_list.html', context)

@login_required
def diagnostico_detail(request, diagnostico_id):
	diagnostico = Diagnostico.objects.get(id=diagnostico_id)
	if request.method == 'GET':
		form = DiagnosticoForm(instance = diagnostico)
	else:
		form = DiagnosticoForm(request.POST, instance=diagnostico)
		if form.is_valid():
			form.save()	
			return redirect('diagnostico_list')
	return render(request, 'consulta/diagnostico_form.html',{'form':form})

@method_decorator(login_required, name='dispatch')
class diagnosticoDelete(DeleteView):

    model=Diagnostico
    success_url = reverse_lazy('diagnostico_list')


#------------------------------------serializers_rest_framework-------------------------------------   
@method_decorator(login_required, name='dispatch')
class MedicoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Medico.objects.all()
    serializer_class = MedicoSerializer
@method_decorator(login_required, name='dispatch')
class DiagnosticoViewSet(viewsets.ModelViewSet): 
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Diagnostico.objects.all()
    serializer_class = DiagnosticoSerializer