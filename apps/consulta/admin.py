from django.contrib import admin
from apps.consulta.models import Medico, Diagnostico
# Register your models here.

admin.site.register(Medico)
admin.site.register(Diagnostico)
