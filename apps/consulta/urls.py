from django.urls import path, include
from apps.consulta import views
from rest_framework import routers


router = routers.DefaultRouter()
router.register('medico_rest', views.MedicoViewSet)
router.register('diagnostico_rest', views.DiagnosticoViewSet)

urlpatterns = [

  #------------------------------------rest_framework-----------------------------------
  path('api_consulta/', include(router.urls)),
  #----------------------------------------Base-----------------------------------------

  path('', views.first_view, name='first_view'),

  #---------------------------------------Medico----------------------------------------

  path('medico_add', views.medico_register, name='medico_register'),

  path('list_medico', views.medico_list, name='medico_list'),

  path('detail_medico/<int:medico_id>/', views.medico_detail, name='medico_detail'),

  path('delete_medico/<int:pk>/', views.medicoDelete.as_view(), name='medicoDelete'),

  
  #------------------------------------Diagnostoco---------------------------------------

  path('diagnostico_add', views.diagnostico_register, name='diagnostico_register'),

  path('list_diagnostico', views.diagnostico_list, name='diagnostico_list'),

  path('detail_diagnostico/<int:diagnostico_id>/', views.diagnostico_detail, name='diagnostico_detail'),

  path('delete_diagnostico/<int:pk>/', views.diagnosticoDelete.as_view(), name='diagnosticoDelete'),






  
]
