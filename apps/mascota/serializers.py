from rest_framework import serializers
from .models import Mascota, Vacuna, TipoMascot

class MascotaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Mascota
        fields = ('__all__')


class VacunaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Vacuna
        fields = ('__all__')

class TipoMascotSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TipoMascot
        fields = ('__all__')
        
