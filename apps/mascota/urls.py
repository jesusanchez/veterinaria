from django.urls import path, include
from apps.mascota import views 
from rest_framework import routers

router = routers.DefaultRouter()
router.register('mascot_rest', views.MascotaViewSet)
router.register('vacuna_rest', views.VacunaViewSet)
router.register('tipoMascot_rest', views.TipoMascotViewSet)

urlpatterns = [

#------------------------------------rest_framework-----------------------------------
    path('api_mascot/',include(router.urls)),
#----------------------------------------Base-----------------------------------------
  
    path('', views.first_view, name='base'),

#--------------------------------------Mascota----------------------------------------

    path('mascot_add', views.mascot_register, name='mascot_register'),

    #ruta para listar los datos de los animales
    path('list_mascot', views.mascot_list, name='mascot_list'),

    #ruta para realizar modificaciones de los datos de las mascotas
    path('detail/<int:mascot_id>/', views.mascot_detail, name='mascot_detail'),

    #ruta para eliminar datos de las mascotas 
    path('delete_mascot/<int:pk>/', views.mascotDelete.as_view(), name='mascot_delete'), 

    
    

#--------------------------------------Vacuna-------------------------------------------
 
    path('vacuna_add', views.vacuna_register, name='vacuna_register'),

    path('list_vacuna', views.vacuna_list, name='vacuna_list'),

    path('detail_vacuna/<int:vacuna_id>/', views.vacuna_detail, name='vacuna_detail'),

    path('delete_vacuna/<int:pk>/', views.vacunaDelete.as_view(), name='vacunaDelete'),
 
 #--------------------------------------Tipo mascota------------------------------------------- 
  
   path('tipoMascot_add', views.tipoMasct_register, name='tipoMasct_register'),
]
