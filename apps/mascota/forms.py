from django import forms
from .models import Mascota, Vacuna, TipoMascot


class VacunaForm(forms.ModelForm):

    class Meta:
        model = Vacuna
        fields=['nombre']

        labels = {'nombre' : 'nombre'
        }
        widgets = {
            'nombre' : forms.TextInput(attrs={'class':'form-control'})
        }


class MascotaForm(forms.ModelForm):

    class Meta:
        #campos qu se van a utilizar
        model = Mascota
        fields = ['tipoMascot', 'raza','nombre', 
        'sexo','edad','fecha_y_horario_de_ingreso','persona', 'vacunas', 'solicitudCita']

        labels = {
                'tipoMascot': 'tipo de mascota',
                'raza': 'raza',    
                'nombre': 'Nombre',
                'sexo': 'Sexo',
                'edad': 'Edad',
                'fecha_y_horario_de_ingreso':'Fecha y horario de ingreso',
                #'foto':'foto',
                'persona': 'Adoptante',
                'vacunas': 'Vacunas',
                'solicitudCita': 'solicitud Cita'
            }
        widgets = {

            'tipoMascot' : forms.Select(attrs={'class':'form-control'}),
            'raza' : forms.TextInput(attrs={'class':'form-control'}),
            'nombre' : forms.TextInput(attrs={'class':'form-control'}),
            'sexo' : forms.TextInput(attrs={'class':'form-control'}),
            'edad_aproximada' : forms.TextInput(attrs={'class':'form-control'}),
            'fecha_y_horario_de_ingreso': forms.DateInput(attrs={'format': 'mm/dd/yyyy','icon': 'fa-calendar'}),
            #'foto' : forms.ImageField(),
            'persona' : forms.Select(attrs={'class':'form-control'}),
            'vacunas' : forms.CheckboxSelectMultiple(),
            'solicitudCita' : forms.Select(attrs={'class':'form-control'}),
            
            
        }    

class TipoMascotForm(forms.ModelForm):

    class Meta:
        model = TipoMascot
        fields=['tipoMascot']

        labels = {'tipoMascot' : 'tipo Mascota'
        }
        widgets = {
            'tipoMascot' : forms.TextInput(attrs={'class':'form-control'})
        }
        