from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from apps.mascota.models import Mascota, Vacuna, TipoMascot
from apps.mascota.forms import MascotaForm, VacunaForm, TipoMascotForm
from django.views.generic import ListView
from django.views.generic import ListView, DetailView
from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from rest_framework import viewsets
from .serializers import MascotaSerializer, VacunaSerializer, TipoMascotSerializer

# Create your views here.

@login_required
def first_view(request):
    return  render(request, 'base.html')
#----------------------------------------------Meascota--------------------------------------------


# Registro de mascota 	
@login_required
def mascot_register(request):
	if request.method == "POST":
		form = MascotaForm(request.POST)
		if form.is_valid():
			mascota_item= form.save(commit=False)
			mascota_item.save()
			return redirect('mascot_list')
	else:
		form = MascotaForm()
		
	return render(request,'mascota/mascota_form.html',{'form':form})
	
#se lista los atributos de las mascotas y su dueño y vacunas
@login_required
def mascot_list(request):
	mascot_list = Mascota.objects.all().order_by('id')
	context = {'objects_list': mascot_list}
	return render(request, 'mascota/mascota_list.html', context)


# se edita los datos de las mascotas 
@login_required
def mascot_detail(request, mascot_id):
	mascota = Mascota.objects.get(id=mascot_id)
	if request.method == 'GET':
		form = MascotaForm(instance = mascota)
	else:
		form = MascotaForm(request.POST, instance=mascota)
		if form.is_valid():
			form.save()	
		return redirect('mascot_list')
	return render(request, 'mascota/mascota_form.html',{'form':form})


@method_decorator(login_required, name='dispatch')
class mascotDelete(DeleteView):

    model=Mascota
    success_url = reverse_lazy('mascot_list')


#----------------------------------------------Vacuna--------------------------------------------
@login_required
def vacuna_register(request):
	if request.method == "POST":
		form = VacunaForm(request.POST)
		if form.is_valid():
			vacuna_item = form.save(commit=False)
			vacuna_item.save()
			return redirect('vacuna_list')			
	else:
		form = VacunaForm
	return render(request, 'mascota/vacuna_form.html',{'form':form})

@login_required
def vacuna_list(request):
	vacuna_list = Vacuna.objects.all().order_by('id')
	context = {'objects_list': vacuna_list}
	return render(request, 'mascota/vacuna_list.html', context)

@login_required
def vacuna_detail(request, vacuna_id):
	vacuna = Vacuna.objects.get(id=vacuna_id)
	if request.method == 'GET':
		form = VacunaForm(instance = vacuna)
	else:
		form = VacunaForm(request.POST, instance=vacuna)
		if form.is_valid():
			form.save()	
		return redirect('vacuna_list')
	return render(request, 'mascota/vacuna_form.html',{'form':form})


@method_decorator(login_required, name='dispatch')
class vacunaDelete(DeleteView):

    model=Vacuna
    success_url = reverse_lazy('vacuna_list')



#----------------------------------------------Tipo Mascota--------------------------------------------
@login_required
def tipoMasct_register(request):
	if request.method == "POST":
		form = TipoMascotForm(request.POST)
		if form.is_valid():
			tipoMascot_item = form.save(commit=False)
			tipoMascot_item.save()
			return redirect('mascot_register')
	else:
		form = TipoMascotForm()

	return render(request, 'mascota/tipoMascota_form.html',{'form': form}) 			


#------------------------------------serializers_rest_framework------------------------------------- 
@method_decorator(login_required, name='dispatch')
class MascotaViewSet(viewsets.ModelViewSet):
	"""
    API endpoint that allows users to be viewed or edited.
    """
	queryset = Mascota.objects.all()
	serializer_class = MascotaSerializer
	
@method_decorator(login_required, name='dispatch')
class VacunaViewSet(viewsets.ModelViewSet):
	"""
    API endpoint that allows users to be viewed or edited.
    """
	queryset = Vacuna.objects.all()
	serializer_class = VacunaSerializer

@method_decorator(login_required, name='dispatch')
class  TipoMascotViewSet(viewsets.ModelViewSet):
	"""
    API endpoint that allows users to be viewed or edited.
    """
	queryset = TipoMascot.objects.all()
	serializer_class = TipoMascotSerializer



