from django.contrib import admin
from apps.mascota.models import Mascota, Vacuna, TipoMascot

# Register your models here.

class MascotaAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')

admin.site.register(Mascota, MascotaAdmin)
admin.site.register(Vacuna)
admin.site.register(TipoMascot)