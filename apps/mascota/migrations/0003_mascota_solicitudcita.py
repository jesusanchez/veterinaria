# Generated by Django 2.1.3 on 2018-11-24 01:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0002_auto_20181123_2019'),
        ('mascota', '0002_auto_20181123_1903'),
    ]

    operations = [
        migrations.AddField(
            model_name='mascota',
            name='solicitudCita',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cliente.SolicitudCita'),
        ),
    ]
