from django.db import models
from django.utils import timezone
from apps.cliente.models import Persona, SolicitudCita
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.urls import reverse

# Create your models here.

class Vacuna(models.Model):
   nombre = models.CharField(max_length=30)

   def __str__(self):
	   return self.nombre

   def get_absolute_url(self):
	   return reverse('vacuna_list')


class TipoMascot(models.Model):
   tipoMascot = models.CharField(max_length=50)

   def __str__(self):
	   return self.tipoMascot

   def get_absolute_url(self):
	   return reverse('mascot_list')	   



class Mascota(models.Model):

	tipoMascot = models.ForeignKey(TipoMascot, null=True, blank=True, on_delete= models.CASCADE)
	raza = models.CharField(max_length=50)
	nombre = models.CharField(max_length=50)
	sexo = models.CharField(max_length=10)
	edad = models.IntegerField()
	fecha_y_horario_de_ingreso = models.DateField()
	foto = models.ImageField(upload_to='mascotaPhotos/')
	persona = models.ForeignKey(Persona, null=True, blank=True, on_delete= models.CASCADE)
	vacunas = models.ManyToManyField(Vacuna, blank=True)
	solicitudCita = models.ForeignKey(SolicitudCita, null=True, blank=True, on_delete= models.CASCADE)

	# se añade la hora en que se creo el archivo
	created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    #se añade la hora en la que fue modificado o se actuliza  el el archivo
	updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")
    

	def __str__(self):
	   return self.nombre

	def get_absolute_url(self):
		return reverse('mascot_list')


	