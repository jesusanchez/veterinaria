from rest_framework import serializers
from .models import Persona, SolicitudCita

class PersonaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Persona
        fields = ('__all__')


class SolicitudCitaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SolicitudCita
        fields = ('__all__')
