from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from apps.cliente.models import Persona,SolicitudCita
from apps.cliente.forms import PersonaForm, SolicitudForm
from django.views.generic import ListView, DetailView
from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from rest_framework import viewsets
from .serializers import PersonaSerializer, SolicitudCitaSerializer


# Create your views here.
@login_required
def first_view(request):
    return  render(request, 'base.html')
#------------------------------------Persona------------------------------------------------

#Registro del cliente
@login_required
def client_register(request):
    if request.method == "POST":
        form = PersonaForm(request.POST)
        if form.is_valid():
            client_item = form.save(commit=False)
            client_item.save()
            return redirect('client_list')
    else:
            form = PersonaForm()
    return render(request,'cliente/client_form.html',{'form':form})

#Se lista los atributos del clinte
@login_required
def client_list(request):
    client_list = Persona.objects.all().order_by('id')
    context = {'objects_list': client_list}
    return render(request, 'cliente/client_list.html', context)

#Permite modificar los datos del cliene
@login_required
def client_detail(request, client_id):
	cliente = Persona.objects.get(id=client_id)
	if request.method == 'GET':
		form = PersonaForm(instance = cliente)
	else:
		form = PersonaForm(request.POST, instance=cliente)
		if form.is_valid():
			form.save()	
		return redirect('client_list')
	return render(request, 'cliente/client_form.html',{'form':form}) 
   
@method_decorator(login_required, name='dispatch')       
class clientDelete(DeleteView):

    model=Persona
    success_url = reverse_lazy('client_list')


#---------------------------------solicitud de cita-----------------------------------------    
@login_required
def solicitudCita_register(request):
    if request.method == "POST":
        form = SolicitudForm(request.POST)
        if form.is_valid():
            solicitudCita_item = form.save(commit=False)
            solicitudCita_item.save()
            return redirect('solicitudCita_list')
    else:
            form = SolicitudForm
    return render(request,'cliente/solicitudCita_form.html',{'form':form})



#Se lista los atributos del solicitudes de las citas 
@login_required
def solicitudCita_list(request):
    solicitudCita_list = SolicitudCita.objects.all().order_by('id')
    context = {'objects_list': solicitudCita_list}
    return render(request, 'cliente/solicitudCita_list.html', context)

@login_required
def solicitudCita_detail(request, solicitudCita_id):
	solicitud = SolicitudCita.objects.get(id=solicitudCita_id)
	if request.method == 'GET':
		form = SolicitudForm(instance = solicitud)
	else:
		form = SolicitudForm(request.POST, instance=solicitud)
		if form.is_valid():
			form.save()	
		return redirect('solicitudCita_list')
	return render(request, 'cliente/solicitudCita_form.html',{'form':form})   


@method_decorator(login_required, name='dispatch')
class solicitudCitaDelete(DeleteView):
	model = SolicitudCita
	success_url = reverse_lazy('solicitudCita_list')




#----------------------------serializers_rest_framework-------------------------------------    
@method_decorator(login_required, name='dispatch')
class PersonaViewSet(viewsets.ModelViewSet):

    queryset = Persona.objects.all()
    serializer_class = PersonaSerializer

@method_decorator(login_required, name='dispatch')
class SolicitudCitaViewSet(viewsets.ModelViewSet):

    queryset = SolicitudCita.objects.all()
    serializer_class = SolicitudCitaSerializer

