from django.contrib import admin
from apps.cliente.models import Persona, SolicitudCita


# Register your models here.
admin.site.register(Persona)
admin.site.register(SolicitudCita)