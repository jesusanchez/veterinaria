from django import forms
from .models import Persona, SolicitudCita

class PersonaForm(forms.ModelForm):

    class Meta:
        #campos qu se van a utilizar
        model = Persona
        fields = ['cedula','nombre','apellido', 'fecha_de_nacimiento','telefono','email','direccion']

        labels = {
                'cedula': 'Cedula',
                'nombre': 'Nombre', 
                'apellido':'Apellidos',
                'fecha_de_nacimiento ':'Fecha de Nacimeinto',  
                'telefono': 'Telefono',
                'email': 'Email',
                'direccion': 'Direccion',
                
            }
        widgets = {

            'cedula' : forms.TextInput(attrs={'class':'form-control'}),
            'nombre' : forms.TextInput(attrs={'class':'form-control'}),
            'apellido' : forms.TextInput(attrs={'class':'form-control'}),
            'fecha_de_nacimiento ' : forms.TextInput(attrs={'class':'form-control'}),
            'telefono' : forms.TextInput(attrs={'class':'form-control'}),
            'email' : forms.TextInput(attrs={'class':'form-control'}),
            'direccion' : forms.TextInput(attrs={'class':'form-control'}),
        }   

class SolicitudForm(forms.ModelForm):

    class Meta:
        model = SolicitudCita
        fields = ['persona','numero_mascota_a_ingresar','razon']

        labels = {
            'persona':'Persona',
            'numero_mascota_a_ingresar' :'Numero de mascotas a ingresar',
            'razon':'razones',

        }

        widgets = {

             'persona' : forms.Select(attrs={'class':'form-control'}),
             'numero_mascota_a_ingresar' : forms.TextInput(attrs={'class':'form-control'}),
             'razon' : forms.Textarea(attrs={'class': 'form-control', 'style': 'resize:none;'}),
             

        }    
            
