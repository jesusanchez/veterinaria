from django.urls import path, include
from apps.cliente import views
from rest_framework import routers 


router = routers.DefaultRouter()
router.register('client_rest', views.PersonaViewSet)
router.register('SolicitudC_rest', views.SolicitudCitaViewSet)


urlpatterns = [

  #------------------------------------rest_framework-----------------------------------
  path('api_client/',include(router.urls)),
  
  #----------------------------------------Base-----------------------------------------

  path('', views.first_view, name='first_view'),

 #----------------------------------------Cliente-----------------------------------------

  path('client_add', views.client_register, name='client_register'),

  path('list_client', views.client_list, name='client_list'),

  path('detail/<int:client_id>/', views.client_detail, name='client_detail'),

  path('delete_client/<int:pk>/', views.clientDelete.as_view(), name='clientDelete'),

 #---------------------------------------solicitudCita------------------------------------ 

  path('solicitudCita_add', views.solicitudCita_register, name='solicitudCita_register'),

  path('list_solicitudCita', views.solicitudCita_list, name='solicitudCita_list'),

  path('detail_Solicitud/<int:solicitudCita_id>/', views.solicitudCita_detail, name='solicitudCita_detail'),

  path('delete_solicitudCita/<int:pk>/', views.solicitudCitaDelete.as_view(), name='solicitudCitaDelete'),

]


