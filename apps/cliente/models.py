from django.db import models

# Create your models here.


class Persona(models.Model):

    cedula = models.CharField(max_length=20)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    fecha_de_nacimiento = models.CharField(max_length=20)
    telefono = models.CharField(max_length=15)
    email= models.EmailField(max_length=100)
    direccion =models.CharField(max_length=50)
    

    def __str__(self):
        return '{} {}'.format(self.nombre,self.apellido)

    def get_absolute_url(self):
        return reverse('client_list')    


class SolicitudCita(models.Model):
    persona = models.ForeignKey(Persona, null=True, blank=True, on_delete=models.CASCADE)
    numero_mascota_a_ingresar = models.IntegerField()
    razon = models.TextField()

    def __str__(self):
        return self.razon

    def get_absolute_url(self):
        return reverse('solicitudCita_list')
	

